#include"stdio.h"
#include"gsl/gsl_matrix.h"

int print_half_00(gsl_matrix* m)
{
	double half = 0.5;
	int status = printf( "half m_{00} = %g\n", gsl_matrix_get(m,0,0)*half );
	return status;
}

int main(void)
{
	gsl_matrix *m = gsl_matrix_alloc(1,1);
	gsl_matrix_set(m,0,0,66);
	printf("half m_{00} (should be 33):\n");
//	Printing result = 33
	int status = print_half_00(m);
/*	Upon successful return, 'printf' returns
	the number of characters printed*/
	if(status=17)
		printf("status=%i : everything went just fine\n", status);
	else
		printf("status=%i : SOMETHING WENT TERRIBLY WRONG\n", status);

	printf("Upon successful return, 'printf' returns the number of characters printed - thus status = 17\n");

	gsl_matrix_free(m);
return 0;
}
