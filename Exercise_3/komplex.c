#include <stdio.h>
#include "komplex.h"
void komplex_print(char *s, komplex a){
	printf ("%s (%.2g,%.2g)\n", s, a.re, a.im);
}

void komplex_set(komplex* z, double x, double y){
	(*z).re = x;
	(*z).im = y;
}

komplex komplex_new(double x, double y){
	komplex z = { x, y };
	return z;
}

komplex komplex_add(komplex a, komplex b){
	komplex result = {a.re + b.re , a.im + b.im};
	return result;
}

komplex komplex_sub(komplex a, komplex b){
	komplex result = {a.re - b.re , a.im - b.im};
	return result;
}

int komplex_equal(komplex a, komplex b){
	int result;
	if(a.re == b.re && a.im == b.im){result = 1;}
	else{result = 0;}
	return result;
}

komplex komplex_mul(komplex a, komplex b){
	komplex result = {a.re*b.re , a.im*b.im};
	return result;
}

komplex komplex_div(komplex a, komplex b){
	komplex result = {a.re/b.re , a.im/b.im};
	return result;
}




