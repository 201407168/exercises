#ifndef HAVE_KOMPLEX_H // this is necessary for multiple includes
// Define structures
struct komplex {double re; double im;};
typedef struct komplex komplex;

//Define functions
	//prints string s and then komplex z
	void komplex_print(char* s, komplex z);
 	//z is set to x+i*y
	void komplex_set(komplex* z, double x, double y);   
	// returns x+i*y
	komplex komplex_new(double x, double y);
	//returns a+b
	komplex komplex_add(komplex a, komplex b);
	//returns a-b
	komplex komplex_sub(komplex a, komplex b);
	//returns 1 if equal, 0 otherwise
	int komplex_equal(komplex a, komplex b);
	//returns a*b
	komplex komplex_mul      (komplex a, komplex b);
	//returns a/b
	komplex komplex_div      (komplex a, komplex b);
	/* //returns complex conjugate
	komplex komplex_conjugate(komplex z);
	komplex komplex_abs      (komplex z);
	komplex komplex_exp      (komplex z);
	komplex komplex_sin      (komplex z);
	komplex komplex_cos      (komplex z);
	komplex komplex_sqrt     (komplex z); */
#define HAVE_KOMPLEX_H
#endif
