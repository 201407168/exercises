#include "komplex.h"
#include "stdio.h"
#define TINY 1e-6

int main(){
	komplex a = {1,2};
	komplex b = {3,4};

	printf("Exercise 3\n");
	komplex r = komplex_add(a,b);
	komplex R = {4,6};
	komplex_print("a =", a);
	komplex_print("b =", b);
	komplex_print("a+b should   = ", R);
	komplex_print("a+b actually = ", r);
	printf("\n");

	printf("The optional part:\n");
	komplex s = komplex_sub(a,b);
	komplex_print("a-b =", s);
	int e = komplex_equal(a,b);
		if(e == 1){printf("Is a = b? Yes\n");}
		else{printf("Is a = b? No\n");}
	komplex m = komplex_mul(a,b);
	komplex_print("a*b =", m);
	komplex d = komplex_div(a,b);
	komplex_print("a/b =", d);
}
