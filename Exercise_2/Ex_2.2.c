#include "tgmath.h"
#include "stdio.h"
#include "limits.h"
#include "float.h"
int main(){
	printf("Exercise 2.2.i\n");
	int max = INT_MAX/3;
	float sum_up_float = 0;	
	for(int i = 1 ; i <= max ; i++)
	{sum_up_float += 1.0/i;}
	printf("sum_up_float = %g\n", sum_up_float);
	
	float sum_down_float = 0;
	for(int i = 1 ; i < max ; i++)
	{sum_down_float += 1.0/(max-i);}
	printf("sum_down_float = %g\n\n", sum_down_float);	

	printf("Exercise 2.2.ii\n");
	printf("The difference...\n\n");

	printf("Exercise 2.2.iii\n");
	printf("The sum converges as a function of max\n\n");

	printf("Exercise 2.2.iv\n");
	double sum_up_double = 0;	
	for(int i = 1 ; i <= max ; i++)
	{sum_up_double += 1.0/i;}
	printf("sum_up_double = %lg\n", sum_up_double);
	
	double sum_down_double = 0;
	for(int i = 1 ; i < max ; i++)
	{sum_down_double += 1.0/(max-i);}
	printf("sum_down_double = %lg\n\n", sum_down_double);

	return 0;
}


