#include "tgmath.h"
#include "stdio.h"
#include "limits.h"
#include "float.h"
int main(){
	printf("Exersice 2.1.i\n");
	printf("The max value from INT_MAX = %i\n", INT_MAX);
	
	int i = 1;
	while(i+1>i){i++;}
	printf("The max value with 'while' = %i\n", i);
	
	int x = 1;
	for( ; x+1>x;){x++;}
	printf("The max value with 'for' = %i\n", x);

	int y = 1;
	do{y++;}
	while(y+1>y);
	printf("The max value with 'do while' = %i\n\n", y);
	
	printf("Exercise 2.1.ii\n");
	printf("The min value from INT_MIN = %i\n", INT_MIN);
	
	int a = 1;
	while(a-1<a){a--;}
	printf("The min value with 'while' = %i\n", a);
	
	int b = 1;
	for( ; b-1<b;){b--;}
	printf("The min value with 'for' = %i\n", b);
	
	int c=1;
	do{c--;}
	while(c-1<c);
	printf("The min value with 'do while' = %i\n\n", c);
	
	printf("Exercise 2.1.iii\n") ;
	
	printf("Following are floats:\n");
	printf("Eps with FLT_EPSILON = %g\n", FLT_EPSILON);
	float df = 1;
	while(1+df!=1){df/=2;} df*=2; // x*=2 betyder x=x*2
	printf("Eps with 'while' = %g\n", df);

	float ef = 1;
	for ( ; 1+ef!=1; ef/=2){} ef*=2;
	printf("Eps with 'for' = %g\n", ef);

	float ff = 1;
	do{ff/=2;}
	while(1+ff!=1); ff*=2;
	printf("Eps with 'do while' = %g\n", ff);

	printf("Following are doubles:\n");
	printf("Eps with DBL_EPSILON = %lg\n", DBL_EPSILON);
	
	double dd = 1;
	while(1+dd!=1){dd/=2;} dd*=2; // x*=2 betyder x=x*2
	printf("Eps with 'while' = %lg\n", dd);

	double ed = 1;
	for ( ; 1+ed!=1; ed/=2){} ed*=2;
	printf("Eps with 'for' = %lg\n", ed);

	double fd = 1;
	do{fd/=2;}
	while(1+fd!=1); fd*=2;
	printf("Eps with 'do while' = %lg\n", fd);

	printf("Following are long doubles:\n");
	printf("Eps with LDBL_EPSILON = %Lg\n", LDBL_EPSILON);	
	
	long double dl = 1;
	while(1+dl!=1){dl/=2;} dl*=2; // x*=2 betyder x=x*2
	printf("Eps with 'while' = %Lg\n", dl);

	long double el = 1;
	for ( ; 1+el!=1; el/=2){} el*=2;
	printf("Eps with 'for' = %Lg\n", el);

	long double fl = 1;
	do{fl/=2;}
	while(1+fl!=1); fl*=2;
	printf("Eps with 'do while' = %Lg\n\n", fl);

	return 0;
}
