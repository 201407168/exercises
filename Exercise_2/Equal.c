#include "tgmath.h"
#include "stdio.h"
#include "limits.h"
#include "float.h"
int equal(double a, double b, double tau, double eps){ 
	int condition;
	double dif = fabs(a-b);
	double absdif = fabs(a-b)/(fabs(a)+fabs(b));
	if(dif < tau || absdif < eps/2){condition = 1;}
	// || betyder or
	else{condition = 0;}

	return condition;
}
