#include <stdio.h>
#include <math.h>
#include <gsl/gsl_integration.h>

double f(double x, void* params){
	//Defining the finction, which is to be integrated
	double F = log(x)/sqrt(x);
	return F;
}


int main(){
	// Defining system
	gsl_function func;
	func.function = &f;
	func.params = NULL;

	double result; //Empty space for pointer
	double error; // Empty space for pointer
	double eps_abs = 1e-6; 
	double eps_rel = 1e-6;
	size_t limit = 100;
	int bounds[2] = {0, 1};
	
	//Defining workspace
	gsl_integration_workspace * ws = 
		gsl_integration_workspace_alloc(limit);

	gsl_integration_qags(
		&func, bounds[0], bounds[1], eps_abs, 	
		eps_rel,limit, ws, &result, &error);

	printf("The integral of ln(x)/sqrt(x) from 0 to 1 is %g\n", result);

	gsl_integration_workspace_free(ws);
	
	return 0;
}
