#include <stdio.h>
#include <tgmath.h>

void integration(double alpha, double*, double*);

int main(){
	double norm_int;
	double H_int;
	// Making for loop running function "integration"
	// for range of alpha-values.
	for(double a = 0.01; a < 2*M_PI; a += 0.01){
	integration(a, &norm_int, &H_int);
	printf("%lg\t %lg\t %lg\t %lg\n", a, norm_int, H_int,
	H_int/norm_int);	
	}
	return 0;
}
