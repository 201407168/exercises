#include <stdio.h>
#include <tgmath.h>
#include <gsl/gsl_integration.h>

double norm(double x, void* params){
	// Defining norm
	double alpha = *(double*) params;
	double f = exp(-alpha*x*x);
	return f;
}

double Hamiltonian(double x, void* params){
	// Defining Hamiltonian
	double alpha = *(double *)params;
	double f =
		(-(alpha*alpha)*(x*x)/2+(alpha/2)+(x*x)/2)*exp(-alpha*x*x);
	return f;
}

void integration(double alpha,double* result_of_norm,double* result_of_H){
	// Defining relevant values
	double eps_abs = 1e-6;
	double eps_rel = 1e-6;
	size_t limit = 1000;
	double result_norm;
	double error_norm;
	double result_H;
	double error_H;

	// Defining norm function
	gsl_function func_norm;
	func_norm.function = norm;
	func_norm.params = &alpha;
	//Defining Hamiltonian function
	gsl_function func_H;
	func_H.function = Hamiltonian;
	func_H.params = &alpha;

	// Defining workspace
	gsl_integration_workspace* ws = 
		gsl_integration_workspace_alloc(limit); 

	// Making the norm integral  
	gsl_integration_qagi(
		&func_norm, eps_abs, eps_rel, limit, ws,
		&result_norm, &error_norm);
	// Making the Hamiltonian integral
	gsl_integration_qagi(
		&func_H, eps_abs, eps_rel, limit, ws,
		&result_H, &error_H);
	
	// Returning the results to the function integration
	(*result_of_norm) = result_norm;
	(*result_of_H) = result_H;

	// Freeing workspace memory
	gsl_integration_workspace_free(ws);
}

