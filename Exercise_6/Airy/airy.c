#include <stdio.h>
#include <gsl/gsl_sf_airy.h>
#include <stdlib.h>
#define mode GSL_PREC_APPROX

int main(int argc, char** argv){
	for(int i=0; i<argc; i++){
	double x = atof(argv[i]);
	printf("%lg\t %lg\t %lg\n",
		x, gsl_sf_airy_Ai(x, mode),
		gsl_sf_airy_Bi(x, mode));
			           }
	return 0;
}
