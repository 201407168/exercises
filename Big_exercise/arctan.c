#include <stdio.h>
#include <tgmath.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multiroots.h>

int func(const gsl_vector* v, void* params, gsl_vector* f){
	double a = gsl_vector_get(v, 0);
	double x = *(double*) params;
	gsl_vector_set(f, 0, (tan(a)-x));
	return GSL_SUCCESS;
}

double solver(double x){
	if(x < 0)
		return -solver(-x);
	
	// Defining function
	int dim = 1;
	gsl_multiroot_function F;
	F.f = func;
	F.n = dim;
	F.params = (void*) &x;
	
	// Allocateing memory and making start guess
	gsl_vector* a = gsl_vector_alloc(dim);
	gsl_vector_set(a, 0, 1);
	
	// Defining algorithm which replaces calls to the jacobian
	// funcion by its finite differentation approximation.
	const gsl_multiroot_fsolver_type *T =
		gsl_multiroot_fsolver_hybrid;
	// Defining workspace
	gsl_multiroot_fsolver *s =
		gsl_multiroot_fsolver_alloc(T, dim);
	// Sets (or reset) a solver 's' to use the function
	// 'F' and the initial guess a
	gsl_multiroot_fsolver_set(s, &F, a);
	
	// Run following loop while the uncertainties are too big.
	// When 'status' is acceptable status=GSLSUCCESS
	// (thus not GSL_CONTINUE) and the number of itera-
	// tions <1000, print out the number of iterations
	int status;
	int iter = 0;
	double acc = 1e-6;
	do{
		iter++;
		gsl_multiroot_fsolver_iterate(s);
		status = gsl_multiroot_test_residual (s->f, acc);
	    }
	while(status == GSL_CONTINUE && iter < 1000);
	if(iter == 1000)
		printf("WARNING: The number of iterations reached it's maximum.\n");
	
	// Find the 'a' values - thus arctan(x).
	double a_res = gsl_vector_get(s->x, 0);
	double result = fmod(a_res, M_PI/2);
	while(result < 0) result += M_PI/2;
	//printf("arctan(x) = %lg\n", result);

	// Freeing mamory
	gsl_multiroot_fsolver_free(s);
	gsl_vector_free(a);

	return result;	
	
}
