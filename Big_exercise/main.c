#include <stdio.h>
#include <tgmath.h>

double solver(double x);

int main(){
	for(double x = -15 ; x < 15; x += 0.5){
	solver(x);
	printf("%lg\t %lg\n", x, solver(x));
	}

	printf("\n\n");

	for(double x = -15 ; x < 15; x += 0.5){
	printf("%lg\t %lg\n", x, atan(x));
	}
	return 0;
}
