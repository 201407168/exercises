#include <stdio.h>
#include "nvector.h"
#include <stdlib.h>
#include <math.h>

nvector* nvector_alloc(int n){
  nvector* v = malloc(sizeof(nvector));
  (*v).size = n;
  (*v).data = malloc(n*sizeof(double));
  if( v==NULL ) fprintf(stderr,"error in nvector_alloc\n");
  return v;
}

void nvector_free(nvector* v){
	free(v->data);
	free(v);
}

void nvector_set(nvector* v, int i, double value){ 
	(*v).data[i]=value;
}

double nvector_get(nvector* v, int i){
	return (*v).data[i];
}

double nvector_dot_product(nvector* u, nvector* v){
	int n = sizeof(u);
	double product = 0;
	for(int i=0; i<n; i++){
	double v1 = (*u).data[i];
	double v2 = (*v).data[i];
	product += v1*v2;
	}
	return product;
}

int double_equal(double a, double b)
{
	double TAU = 1e-6, EPS = 1e-6;
	if (fabs(a - b) < TAU)
		return 1;
	if (fabs(a - b) / (fabs(a) + fabs(b)) < EPS / 2)
		return 1;
	return 0;
}

/*int double_equal(double a, double b){
	int result;
	if(a == b){result = 1;}
	else{result = 0;}
	return result;
}*/
