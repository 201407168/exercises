#ifndef HAVE_NVECTOR_H /* for multiple includes */

typedef struct {int size; double* data;} nvector;
	//allocates memory for size-n vector
	nvector* nvector_alloc(int n);
	//frees memory
	void nvector_free(nvector* v);
	//v_i ← value
	void nvector_set(nvector* v, int i, double value);
	//returns v_i
	double nvector_get(nvector* v, int i);
	//returns dot-product
	double nvector_dot_product(nvector* u, nvector* v);
	//returns 1 if equal, otherwise 0
	int double_equal(double a, double b);

	/* //prints s and then vector
	void nvector_print    (char* s, nvector* v);
	//all elements ← 0
	void nvector_set_zero (nvector* v);
	//1, if equal, 0 otherwisw
	int  nvector_equal    (nvector* a, nvector* b);
	//a_i ← a_i + b_i
	void nvector_add      (nvector* a, nvector* b);
	//a_i ← a_i - b_i
	void nvector_sub      (nvector* a, nvector* b);
	//a_i ← x*a_i
	void nvector_scale    (nvector* a, double x); */

#define HAVE_NVECTOR_H
#endif
