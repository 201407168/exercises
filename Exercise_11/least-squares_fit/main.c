#include <stdio.h>
#include <stdlib.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multimin.h>
typedef struct {int n; double *t, *y, *e;} exp_data;

double chi_squared(const gsl_vector* v, void* params){
	double A = gsl_vector_get(v, 0);
	double T = gsl_vector_get(v, 1);
	double B = gsl_vector_get(v, 2);
	exp_data *p = (exp_data*) params;
//	Getting values from exp_data by pointing
	int n = p->n;
	double* t = p->t;
	double* y = p->y;
	double* e = p->e;

//	Defining function
	double f(double t) {return A*exp(-(t)/T) + B;}	
//	Calculating chi²
	double sum = 0;
	for(int i = 0; i<n; i++){
		sum += pow( (f(t[i]) - y[i])/e[i], 2);
			     }
	return sum;
}


int main(){
	double t[] = {0.47, 1.41, 2.36, 3.30, 4.24, 5.18, 6.13, 7.07, 8.01, 8.95};
	double y[] = {5.49, 4.08, 3.54, 2.61, 2.09, 1.91, 1.55, 1.47, 1.45, 1.25};
	double e[] = {0.26, 0.12, 0.27, 0.10, 0.15, 0.11, 0.13, 0.07, 0.15, 0.09};
	int n = sizeof(t)/sizeof(t[0]);	
	
	printf("Experimental data:\nt[i]\t y[i]\t e[i]\n");
	printf("\n\n");
	for(int i = 0; i<n; i++){
		printf("%lg\t %lg\t %lg\n",t[i],y[i],e[i]);
	}
	printf("\n\n");

//	Defining parameters
	exp_data params;
	params.n = n;
	params.t = t;
	params.y = y;
	params.e = e;

	int dim = 3;
//	Defining system
	gsl_multimin_function func;
	func.f = chi_squared;
	func.n = dim;
	func.params = (void*) &params;
	
//	Inserting values/guesses into x-vector
	double x_init[3] = {3, 2, 1};
	gsl_vector *x = gsl_vector_alloc(dim);
	gsl_vector_set (x, 0, x_init[0]);
	gsl_vector_set (x, 1, x_init[1]);
	gsl_vector_set (x, 2, x_init[2]);
	
//	Inserting start values into step-vector
	double step_init[3] = {0.1, 0.1, 0.1};
	gsl_vector *step = gsl_vector_alloc(dim);
	gsl_vector_set(step, 0, step_init[0]);
	gsl_vector_set(step, 1, step_init[1]);
	gsl_vector_set(step, 2, step_init[2]);

/*	Defining algorithm which replaces calls to the jacobian
	funcion by its finite differentation approximation. */
	const gsl_multimin_fminimizer_type *type =
		gsl_multimin_fminimizer_nmsimplex2;
//	Defining workspace
	gsl_multimin_fminimizer *s =
		gsl_multimin_fminimizer_alloc(type, dim);
/*	Sets (or reset) a solver 's' to use the function
	'f' and the initial guess from x. */
	gsl_multimin_fminimizer_set(s, &func, x, step);
	
	int status;
	int iter = 0;
	double acc = 1e-2;
/*	Run following loop while 'status' is unacceptable.
	When 'status' is acceptable status=GSLSUCCESS
	(thus not GSL_CONTINUE) and the number of itera-
	tions <1000, print out the number of iterations. */
	do{
		iter++;
		gsl_multimin_fminimizer_iterate(s);
		status = gsl_multimin_test_size(s->size, acc);
	    }
	while(status == GSL_CONTINUE && iter < 1000);
	if(iter == 1000){printf("Minimizer exceeded maximum number of iterations");}
//	else{printf("Number of iterations = %i\n\n", iter);}

/*	Find the coordinates where the minimum is located
	and print the coordinates. */
	double A = gsl_vector_get(s->x, 0);
	double T = gsl_vector_get(s->x, 1);
	double B = gsl_vector_get(s->x, 2);

//	
	printf("Fit:\nt\t f(t)\n");
	printf("\n\n");
	double f(double t){return A*exp(-(t)/T) + B;}
	for(double i = t[0]; i < t[n-1]+0.1; i += 0.05){
		printf("%lg\t %lg\n", i, f(i));}
	printf("\n\n");
	printf("Minimum of function found at:\n A = %lg\t T = %lg\t B = %lg\n", A, T, B);

//	Freeing memory
	gsl_multimin_fminimizer_free(s);
	gsl_vector_free(step);	
	gsl_vector_free(x);

	return 0;
}
