#include <stdio.h>
#include <stdlib.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multimin.h> 

double rosenbrock(const gsl_vector* v, void* params){	
	const double x = gsl_vector_get(v, 0);
	const double y = gsl_vector_get(v, 1);
	return pow((1-x),2) + 100*pow((y-x*x),2);
}

int main(void){
	int dim = 2;
	// Defining function
	gsl_multimin_function func;
	func.f = rosenbrock;
	func.n = dim;
	func.params = NULL;
	
	// Inserting values/guesses into x-vector
	double x_init[2] = {2.0, -2.0};
	gsl_vector *x = gsl_vector_alloc(dim);
	gsl_vector_set (x, 0, x_init[0]);
	gsl_vector_set (x, 1, x_init[1]);
	
	// Inserting start values into step-vector
	double step_init[2] = {0.1, 0.1};
	gsl_vector *step = gsl_vector_alloc(dim);
	gsl_vector_set(step, 0, step_init[0]);
	gsl_vector_set(step, 1, step_init[1]);

	// Defining algorithm which replaces calls to the jacobian
	// funcion by its finite differentation approximation.
	const gsl_multimin_fminimizer_type *T =
		gsl_multimin_fminimizer_nmsimplex2;
	// Defining workspace
	gsl_multimin_fminimizer *s =
		gsl_multimin_fminimizer_alloc(T, dim);

	// Sets (or reset) a solver 's' to use the function
	// 'f' and the initial guess x
	gsl_multimin_fminimizer_set(s, &func, x, step);
	
	int status;
	int iter = 0;
	double acc = 1e-6;
	// Run following loop while 'status' is unacceptable.
	// When 'status' is acceptable status=GSLSUCCESS
	// (thus not GSL_CONTINUE) and the number of itera-
	// tions <1000, print out the number of iterations
	printf("The algorithm estimated the function at:\n\t x\t y\n");
	do{
		iter++;
		gsl_multimin_fminimizer_iterate(s);
		double cal_x = gsl_vector_get(s->x, 0);
		double cal_y = gsl_vector_get(s->x, 1);
		printf("%10lg %10lg\n", cal_x, cal_y);
		status = gsl_multimin_test_size(s->size, acc);
	    }
	while(status == GSL_CONTINUE && iter < 1000);
	if(iter == 1000){
		printf("Minimizer exceeded maximum number of iterations");}
	else{printf("Number of iterations = %i\n", iter);}

	// Find the coordinates where the minimum is located
	// and print the coordinates
	double result_x = gsl_vector_get(s->x, 0);
	double result_y = gsl_vector_get(s->x, 1);
	printf("Minimum of Rosenbrock function at:\nx = %lg, y = %lg\n", result_x, result_y);

	// Freeing memory
	gsl_multimin_fminimizer_free(s);
	gsl_vector_free(step);	
	gsl_vector_free(x);

	return 0;
}
