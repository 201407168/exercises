#include "stdio.h"
#include "tgmath.h"
#define pi 3.1415926535
int main(){
          int x = tgamma(5);
	  printf("Value of gamma-function(5) = %i\n", x);
	  double y = j1(0.5);
	  printf("Value of Bessel function(5) = %lf\n", y);
	  complex double q = csqrt(-2);
	  printf("Value of sqrt(-2) = %lf + i%lf\n", creal(q), cimag(q));
	  complex double e1 = exp(I);
          printf("Value of exp(i) = %lf + i%lf\n", creal(e1), cimag(e1));
          complex double e2 = exp(pi*I);
          printf("Value of exp(pi*i) = %lf + i%lf\n", creal(e2), cimag(e2)); 
	  complex double e3 = cpow(I, exp(1));
	  printf("Value of I^e = %lf+i%lf\n", creal(e3), cimag(e3));  
	  
	  float f = 0.111111111111111111111111111111;
	  double d = 0.111111111111111111111111111111;
	  long double l = 0.111111111111111111111111111111L;
	  printf("Float = %.25f \n", f);
	  printf("Double = %.25lf\n", d);
	  printf("Long Double = %.25Lf\n", l);
	  return 0;
}
