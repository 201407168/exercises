#include <stdio.h>
#include <stdlib.h>
#include <tgmath.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multiroots.h> 

int rosenbrock(const gsl_vector* v, void* params, gsl_vector* func){	
	const double x = gsl_vector_get(v, 0);
	const double y = gsl_vector_get(v, 1);
	const double dfdx = -2+2*x+400*(x*y+x*x*x);
	const double dfdy = 200*(y-x*x);
	gsl_vector_set(func, 0, dfdx);
	gsl_vector_set(func, 1, dfdy);
	return GSL_SUCCESS;
}

int main(void){
	// Defining function
	gsl_multiroot_function func;
	func.f = rosenbrock;
	func.n = 2; // Dimensions
	func.params = NULL;
	
	// Defining array and inserting values/guesses into
	// x-vector
	int x_init[2] = {2, -2};
	gsl_vector *x = gsl_vector_alloc(2);
	gsl_vector_set (x, 0, x_init[0]);
	gsl_vector_set (x, 1, x_init[1]);	

	// Defining algorithm which replaces calls to the jacobian
	// funcion by its finite differentation approximation.
	const gsl_multiroot_fsolver_type *T =
		gsl_multiroot_fsolver_hybrids;
	// Defining workspace
	gsl_multiroot_fsolver *s =
		gsl_multiroot_fsolver_alloc(T, 2);
	// Sets (or reset) a solver 's' to use the function
	// 'f' and the initial guess x
	gsl_multiroot_fsolver_set(s, &func, x);
	
	// Defining usefull values
	int status;
	int iter = 0;
	double acc = 1e-7;
	// Run following loop while the uncertainties are too big.
	// When 'status' is acceptable status=GSLSUCCESS
	// (thus not GSL_CONTINUE) and the number of itera-
	// tions <1000, print out the number of iterations
	do{
		iter++;
		status = gsl_multiroot_fsolver_iterate(s);
		// Tests the residual value f against the absolute
		// error bound 'acc'. The test returns GSL_SUCCESS
		// if the following condition is achieved,
		status = gsl_multiroot_test_residual (s->f, acc);
	    }
	while(status == GSL_CONTINUE && iter < 1000);
	printf("Number of iterations = %i\n", iter);

	// Find the coordinates where the minimum is lcoated
	// and print the coordinates
	double result_x = gsl_vector_get(s->x, 0);
	double result_y = gsl_vector_get(s->x, 1);
	printf("Minimum of Rosenbrock function at:\nx = %lg, y = %lg\n"
		, result_x, result_y);

	// Freeing memory
	gsl_multiroot_fsolver_free(s);
	gsl_vector_free(x);

	return 0;
}
