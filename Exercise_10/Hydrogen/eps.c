#include <stdio.h>
#include <tgmath.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_odeiv2.h>
#include <gsl/gsl_multiroots.h>
#include <assert.h>
#include <gsl/gsl_errno.h>

double Hydrogen(double E, double r);

int func(const gsl_vector* v, void* params, gsl_vector* f){
	// ???
	double eps = gsl_vector_get(v, 0);
	//assert(eps<0);	
	double rmax=*(double*) params;
	gsl_vector_set(f, 0, Hydrogen(eps, rmax));
	return GSL_SUCCESS;
}

double eps(double rmax){
	// Defining relevant values
	int dim = 1;

	// Defining function
	gsl_multiroot_function F;
	F.f = func;
	F.n = dim;
	F.params = (void*) &rmax;
	
	// ???
	gsl_vector* v = gsl_vector_alloc(dim);
	gsl_vector_set(v, 0, -1.);

	// Defining algorithm which replaces calls to the jacobian
	// funcion by its finite differentation approximation.
	const gsl_multiroot_fsolver_type *T =
		gsl_multiroot_fsolver_hybrid;
	// Defining workspace	
	gsl_multiroot_fsolver *s =
		gsl_multiroot_fsolver_alloc(T, dim);
	
	// Sets (or reset) a solver 's' to use the function
	// 'f' and the initial guess x
	gsl_multiroot_fsolver_set(s, &F, v);
	
	// Defining usefull values
	int status;
	int iter = 0;
	double acc = 1e-3;
	// Run following loop while 'status' is unacceptable.
	// When 'status' is acceptable status=GSLSUCCESS
	// (thus not GSL_CONTINUE) and the number of itera-
	// tions <1000, print out the number of iterations
	do{
		iter++;
		gsl_multiroot_fsolver_iterate(s);
		// Tests the residual value f against the absolute
		// error bound 'acc'. The test returns GSL_SUCCESS
		// if the following condition is achieved,
		status = gsl_multiroot_test_residual (s->f, acc);
	    }
	while(status == GSL_CONTINUE && iter < 1000);
	// Find the coordinates where the minimum is lcoated
	// and print the coordinates
	double epsilon_0 = gsl_vector_get(s->x, 0);
	printf("Epsilon_0 = %lg\n\n\n", epsilon_0);

	gsl_multiroot_fsolver_free(s);
	gsl_vector_free(v);

	return epsilon_0;
}


