#include <stdio.h>
#include <tgmath.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_odeiv2.h>
#include <gsl/gsl_multiroots.h>
#include <assert.h>
#include <gsl/gsl_errno.h>

double Hydrogen(double E, double r);

double eps(double rmax);

int main(int argc, char** argv){
	double rmax = 8.0;
	double E = eps(rmax);
	for(double r = 0; r <= rmax; r += rmax/64)
		printf("%lg\t %lg\t %lg\n", r, Hydrogen(E, r), 			r*exp(-r));

	return 0;
}
