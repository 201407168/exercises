#include <stdio.h>
#include <tgmath.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_odeiv2.h>
#include <gsl/gsl_multiroots.h>
#include <assert.h>
#include <gsl/gsl_errno.h>

int funct(double r, const double y[], double f[], void *params){
	double E = *(double*) params;
	f[0] = y[1];
	f[1] = -2*(E+1/r)*y[0];
	return GSL_SUCCESS;
}

double Hydrogen(double E, double r){
	// The initial condition is: F(r->0)=r-r² so
	double rmin = 1e-3;
	if(r<rmin) return r-r*r;	

	// Defining system
	gsl_odeiv2_system sys;
	sys.function = funct;
	sys.jacobian = NULL;
	sys.dimension = 2;
	sys.params = (void*) &E;
	
	// Setting initial step size and size of relative errors
	// and combining evolution, control and stepper object
	double hstart = 1e-3;
	double eps_abs = 1e-6;
	double eps_rel = 1e-6;
	gsl_odeiv2_driver* driver =
		gsl_odeiv2_driver_alloc_y_new(
		&sys, gsl_odeiv2_step_rkf45,  hstart, eps_abs, 	
		eps_rel);

	// Setting starting point and r_0 = 1e-3 and defining
	// the 
	double r_0 = rmin;
	double y[2] = {r_0-r_0*r_0, 1-2*r_0};
	// Evolving driver system from r_0 to 0 with y
	// containing the values of dependent variables at
	// point r_0. 
	gsl_odeiv2_driver_apply(driver, &r_0, r, y);
	
	//Freeing allocated memory
	gsl_odeiv2_driver_free(driver);
	
	// Returning the result
	return y[0];
}



