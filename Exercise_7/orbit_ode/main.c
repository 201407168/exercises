#include <stdio.h>
#include <math.h> 

double orbit(double phi, double eps, double dudphi);

int main(){
	for(double phi=0; phi<=2*M_PI+1.; phi+=0.1)
		printf("%g\t %g\n", phi, orbit(phi, 0.00, 0.0));
	printf("\n\n");
	for(double phi=0; phi<=2*M_PI+1.; phi+=0.1)
		printf("%g\t %g\n", phi, orbit(phi, 0.00, -0.5));
	printf("\n\n");
	for(double phi=0; phi<=10*M_PI+1.; phi+=0.1)
		printf("%g\t %g\n", phi, orbit(phi, 0.01, -0.5));
	return 0;
}
