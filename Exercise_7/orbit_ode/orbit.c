#include <stdio.h>
#include <math.h>
#include <gsl/gsl_odeiv2.h>
#include <gsl/gsl_errno.h> 

int func(double phi, const double u[], double dudphi[], void* params){
	double eps = *(double*) params;
	dudphi[0] = u[1];
	dudphi[1] = 1-u[0]+eps*u[0]*u[0];
	return GSL_SUCCESS;
}

double orbit(double phi, double eps, double dudphi){
	
	// Defining equation
	gsl_odeiv2_system sys;
	sys.function = func;
	sys.dimension = 2;
	sys.params = (void*) &eps;
	
	// Setting initial step size and size of relative errors
	// and combining evolution, control and stepper object
	double hstart = 1e-3;
	double eps_abs = 1e-6;
	double eps_rel = 1e-6;
	gsl_odeiv2_driver* driver =
		gsl_odeiv2_driver_alloc_y_new(
		&sys, gsl_odeiv2_step_rk8pd,  hstart, eps_abs, eps_rel);
	// Setting starting point and y = 1.
	// Evolving driver system from phi_0 to phi with u
	// containing the values of dependent variables at
	// point phi_0. 
	double phi_0 = 0;
	double y[2] = {1, dudphi};	
	// Evolving driver system from phi_0 to phi, with u
	// containing
	gsl_odeiv2_driver_apply(driver, &phi_0, phi, y);
	
	//Freeing allocated memory
	gsl_odeiv2_driver_free(driver);

	return y[0];
}
