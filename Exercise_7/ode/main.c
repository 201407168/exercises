#include <stdio.h>
#include <tgmath.h>

double ode(double x);

int main(){
	for(double x=0; x<3; x+=0.1)
		printf("%g\t %g\t %g\n", x, ode(x), 1/(1+exp(-x)));
	return 0;
}
