#include <stdio.h>
#include <math.h>
#include <gsl/gsl_odeiv2.h>
#include <gsl/gsl_errno.h> 

int func(double x, const double y[], double dydx[], void* params){
	dydx[0] = y[0]*(1 - y[0]);
	return GSL_SUCCESS;
}

double ode(double x){
	// Defining equation
	gsl_odeiv2_system sys;
	sys.function = func;
	sys.dimension = 1;
	/* When solving a system of equations the Jacobian matrix
	 can be used 'int jacobian(double x, const double y[],
	 double* dfdy[], double* dfdx[],  void* params)', where
	 the function stores the vector of derivative elements;
	 but then it is necessary to define sys.jacobian, and
	 sys.params as well.*/
	
	// Setting initial step size and size of relative errors
	// and combining evolution, control and stepper object
	double hstart = copysign(0.1, x);
	double acc = 1e-6;
	double eps = 1e-6;
	gsl_odeiv2_driver* driver =
		gsl_odeiv2_driver_alloc_y_new(
		&sys, gsl_odeiv2_step_rkf45,  hstart, acc, eps);
	// Setting starting point and ???
	// Evolving driver system from x_0 to x with y containing
	// the values of dependent variables at point x_0. 
	double x_0 = 0;
	double y[1] = {0.5};	
	// Evolving driver system from x0 to x, with y containing
	gsl_odeiv2_driver_apply(driver, &x_0, x, y);
	
	//Freeing allocated memory
	gsl_odeiv2_driver_free(driver);

	return y[0];
}
